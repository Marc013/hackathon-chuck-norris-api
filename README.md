# chuck-norris-graphql

## NPM commands
* `start`: use to start the deployed application on an Azure web app. This will work on an Azure web app only, since `./index.js` is there available only.
* `watch`: auto-compile TypeScript when making changes
* `serve:develop`: serve your compiled files during development
* `lint`: lint your files

## User stories

### #1: quick deployment

The customer needs new Chuck Norris jokes online on a regular basis. To improve your time to release and quality you need a pipeline to lint, build and deploy your Chuck Norris API.

**Requirements:**

* Use Bitbucket pipelines
* Use a pipeline to lint, build and deploy the API (start with “test” * environment only. Nice to have for “staging” and “production”)
* Use “test” branch for integrating your features
* No usernames or passwords in code

### #2: auto PR to “staging”

After checking your feature on the “test” environment you want to merge it ASAP to the “staging” environment. Automatically create a pull request (PR) from your feature to “staging”.

**Requirements:**

* Use “test” branch for integrating your features
* Automate creating PRs from feature branch into "staging" branch, so * developers don't forget to merge code
* No usernames or passwords in code

### #3: prep a demo

We all need some fun after hard work. Make everyone laugh with your best Chuck Norris joke.

**Requirements:**

* Demo will be at the end of the hackathon
* Demo should take 6 minutes at max
* Demo your pipeline and show the requirements fromuser story #1 and #2
* Show the added joke on the deployed GraphiQl interface (already built into the API)

### #4: version bump (bonus)

When merging a feature into "staging" make sure the version number in package.json is bumped.

**Requirements:**

* Developers are human also and forget arbitrary stuff, make sure the version is bumped
* Bump minor version upon a "feature" branch
* Bump patch version upon a "bugfix" branch
