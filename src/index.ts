// Enable sourcemaps
import 'source-map-support/register';

import { graphiqlExpress, graphqlExpress } from 'apollo-server-express';
import bodyParser from 'body-parser';
import cli from 'cli';
import express from 'express';
import helmet from 'helmet';
import schema from './data/schema';

const GRAPHQL_PORT = process.env.PORT || 3000;

const graphQLServer = express();

graphQLServer.use(helmet());
graphQLServer.use('/graphql', bodyParser.json(), graphqlExpress({ schema }));
graphQLServer.use('/graphiql', graphiqlExpress({ endpointURL: '/graphql' }));

graphQLServer.listen(GRAPHQL_PORT, () =>
  cli.info(
    `GraphiQL is now running on http://localhost:${GRAPHQL_PORT}/graphiql`
  )
);
